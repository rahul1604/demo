//package com.rahul.thermokmmsample.utils.observer
//
//import com.rahul.thermokmmsample.model.reader.IThermostatCommunication
//import com.rahul.thermokmmsample.model.reader.ThermostatReader
//
//fun ThermostatReader.Companion.create(communication: IThermostatCommunication) =
//    ThermostatReader(communication = communication)