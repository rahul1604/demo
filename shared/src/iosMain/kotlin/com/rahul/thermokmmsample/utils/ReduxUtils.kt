package com.rahul.thermokmmsample.utils

import com.rahul.thermokmmsample.store.ThermostatFeed
import com.rahul.thermokmmsample.utils.observer.wrap

fun ThermostatFeed.watchState() = observeState().wrap()
fun ThermostatFeed.watchSideEffect() = observeSideEffect().wrap()