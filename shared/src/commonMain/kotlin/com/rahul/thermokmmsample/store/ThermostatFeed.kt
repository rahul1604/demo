package com.rahul.thermokmmsample.store

import com.rahul.thermokmmsample.model.ThermostatModel
import com.rahul.thermokmmsample.model.reader.ThermostatReader
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

data class TState(
    val progress: Boolean,
    val feeds: List<ThermostatModel>,
) : State

sealed class TAction : Action {
    data class Refresh(val forceLoad: Boolean) : TAction()
    data class Data(val feeds: List<ThermostatModel>) : TAction()
    data class Error(val error: Exception) : TAction()
}

sealed class TSideEffect : Effect {
    data class Error(val error: Exception) : TSideEffect()
}

class ThermostatFeed(private val thermostatReader: ThermostatReader) :
        Store<TState, TAction, TSideEffect>,
        CoroutineScope by CoroutineScope(
            Dispatchers.Main
        ) {

    private val state = MutableStateFlow(TState(false, emptyList()))
    private val sideEffect = MutableSharedFlow<TSideEffect>()

    override fun observeState(): StateFlow<TState> = state

    override fun observeSideEffect(): Flow<TSideEffect> = sideEffect

    override fun dispatch(action: TAction) {
        val oldState = state.value
        val newState = when (action) {
            is TAction.Refresh -> {
                if (oldState.progress) {
                    launch { sideEffect.emit(TSideEffect.Error(Exception("In progress"))) }
                    oldState
                } else {
                    launch { loadAllFeeds() }
                    oldState.copy(progress = true)
                }
            }

            is TAction.Data -> {
                if (oldState.progress) {
                    TState(false, action.feeds)
                } else {
                    launch { sideEffect.emit(TSideEffect.Error(Exception("Unexpected action"))) }
                    oldState
                }
            }

            is TAction.Error -> {
                if (oldState.progress) {
                    launch { sideEffect.emit(TSideEffect.Error(action.error)) }
                    TState(false, oldState.feeds)
                } else {
                    launch { sideEffect.emit(TSideEffect.Error(Exception("Unexpected action"))) }
                    oldState
                }
            }
        }
        if (newState != oldState) {
            state.value = newState
        }
    }

    private suspend fun loadAllFeeds() {
        try {
            val request = ""
            val allFeeds = thermostatReader.getAll(request)
            dispatch(TAction.Data(allFeeds))
        } catch (e: Exception) {
            dispatch(TAction.Error(e))
        }
    }
}


