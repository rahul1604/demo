package com.rahul.thermokmmsample.model

import com.rahul.thermokmmsample.attr.StringAttr

data class ThermostatModel(val name: StringAttr)