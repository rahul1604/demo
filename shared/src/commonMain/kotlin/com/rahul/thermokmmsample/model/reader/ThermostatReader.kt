package com.rahul.thermokmmsample.model.reader

import com.rahul.thermokmmsample.attr.AttrMode
import com.rahul.thermokmmsample.attr.StringAttr
import com.rahul.thermokmmsample.model.ThermostatModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

interface IThermostatCommunication {
    suspend fun getResponse(request: String): String
}

class ThermostatReader(private val communication: IThermostatCommunication) {

    suspend fun getAll(request: String): List<ThermostatModel> {
        val response = communication.getResponse(request)
        // TODO: this should be leap response parsing
        return response.split(",")
            .map {
                ThermostatModel(name = StringAttr(mode = AttrMode.RW)
                    .also { stringAttr ->
                        stringAttr.str = it

                    }.also {stringAttr ->

                        GlobalScope.launch(Dispatchers.Main) {
                            delay(5000L) // non-blocking delay for 1 second (default time unit is ms)
                            stringAttr.str = stringAttr.str + " Updated!"
                        }
                    })
            }
    }
}