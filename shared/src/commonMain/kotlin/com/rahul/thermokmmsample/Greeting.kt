package com.rahul.thermokmmsample

interface Greet {
    fun getMessage() : String
}

class Greeting {
    fun greeting(greet: Greet): String {
        return greet.getMessage()
    }
}