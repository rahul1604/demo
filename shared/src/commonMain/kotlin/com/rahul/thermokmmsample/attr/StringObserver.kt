package com.rahul.thermokmmsample.attr

interface IView

interface Attribute {
    var mode: AttrMode
}

interface StringObserver : IView {
    fun configure(string: StringAttr)
}

interface ILabel : IView, StringObserver {
    // TODO: interface methods in kotlin with body
    fun bindView(string: StringAttr) {
        string.addObserver(this)
    }
}