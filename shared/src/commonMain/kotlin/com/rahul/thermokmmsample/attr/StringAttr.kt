package com.rahul.thermokmmsample.attr

class StringAttr(override var mode: AttrMode) : Attribute {

    private var observers: MutableList<StringObserver> = mutableListOf()

    var str: String = ""
        set(value) {
            field = value
            if (mode.value >= AttrMode.R.value) {
                observers.forEach { it.configure(this) }
            }
        }

    fun addObserver(observer: StringObserver) {
        if (mode.value >= AttrMode.R.value) {
            observer.configure(this)
        } else {
            print("No OP, should have readable permission.")
        }
        observers.add(observer)
    }

}