package com.rahul.thermokmmsample.attr

enum class AttrMode(val value: Int) {
    INVALID(1),
    R(1),
    W(2),
    RW(3);

    companion object {
        fun fromInt(value: Int) = AttrMode.values().first { it.value == value }
    }
}