pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
}

rootProject.name = "Thermo_KMM_Sample"
include(":androidApp")
include(":shared")