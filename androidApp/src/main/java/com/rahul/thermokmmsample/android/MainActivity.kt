package com.rahul.thermokmmsample.android

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.rahul.thermokmmsample.Greeting
import android.widget.TextView
import com.rahul.thermokmmsample.Greet

class Demo : Greet {

    override fun getMessage(): String {
        return "Demo Message"
    }

}

fun greet(): String {
    return Greeting().greeting(Demo())
}

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tv: TextView = findViewById(R.id.text_view)
        tv.text = greet()
    }
}
