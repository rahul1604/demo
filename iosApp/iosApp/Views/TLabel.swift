//
//  TLabel.swift
//  iosApp
//
//  Created by Rahul Kapoor on 15/08/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import UIKit
import shared
import SwiftUI


class TLabel : ILabel {
    
    var text : Text
    
    init(string: StringAttr) {
        self.text = Text(string.str)
        bindView(string: string)
    }
    
    func bindView(string: StringAttr) {
        string.addObserver(observer: self)
    }
    
    
    func configure(string: StringAttr) {
        text = Text(string.str)
        print("Swift - \(string.str)")
    }

}
