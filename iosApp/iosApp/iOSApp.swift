import SwiftUI

@main
struct iOSApp: App {
    
    let tManager = TManager()
    
    var body: some Scene {
        WindowGroup {
            RootView().environmentObject(tManager.store)
        }
    }
}
