//
//  FeedRow.swift
//  iosApp
//
//  Created by Rahul Kapoor on 15/08/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import SwiftUI
import shared

struct FeedRow: View {
    
    let feed: ThermostatModel
    
    private enum Constants {
        static let imageWidth: CGFloat = 20.0
    }
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 5.0) {
            TLabel(string: feed.name).text.bold().font(.title3).lineLimit(1)
        }
    }
}
