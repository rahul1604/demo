//
//  TManager.swift
//  iosApp
//
//  Created by Rahul Kapoor on 15/08/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation
import shared

class TManager : IThermostatCommunication {
    
    lazy var tReader = ThermostatReader(communication: self)
    lazy var store = ObservableTStore(store: ThermostatFeed(thermostatReader: tReader))
    
    // MARK: IThermostatCommunication method
    func getResponse(request: String, completionHandler: @escaping (String?, Error?) -> Void) {
        // TODO: make network call and get the response
        let thermoResponse = "Thermo 1, Thermo 2, Thermo 3, Thermo 4"
        completionHandler(thermoResponse, nil)
    }
}
