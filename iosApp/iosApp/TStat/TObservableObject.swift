//
//  TObservableObject.swift
//  iosApp
//
//  Created by Rahul Kapoor on 15/08/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation
import shared

// TODO: ObservableObject is only available on iOS 13+
// This is just for demo, we can put our own observer
class ObservableTStore: ObservableObject {
    
    @Published public var state: TState =  TState(progress: false, feeds: [])
    @Published public var sideEffect: TSideEffect?
    
    let store: ThermostatFeed
    
    var stateWatcher : Closeable?
    var sideEffectWatcher : Closeable?

    init(store: ThermostatFeed) {
        self.store = store
        stateWatcher = self.store.watchState().watch { [weak self] state in
            self?.state = state
        }
        sideEffectWatcher = self.store.watchSideEffect().watch { [weak self] state in
            self?.sideEffect = state
        }
    }
    
    public func dispatch(_ action: TAction) {
        store.dispatch(action: action)
    }
    
    deinit {
        stateWatcher?.close()
        sideEffectWatcher?.close()
    }
}
