import SwiftUI
import shared

class DemoGreet : Greet {
    
    func getMessage() -> String {
        "Demo 123"
    }
}

struct ContentView: View {
    let greet = Greeting().greeting(greet: DemoGreet())

	var body: some View {
		Text(greet)
	}
}

struct ContentView_Previews: PreviewProvider {
	static var previews: some View {
		ContentView()
	}
}
