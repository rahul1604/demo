//
//  ThermoListView.swift
//  iosApp
//
//  Created by Rahul Kapoor on 15/08/21.
//  Copyright © 2021 orgName. All rights reserved.
//

import Foundation
import shared
import SwiftUI

struct ThermoListView: ConnectedView {
    
    struct Props {
        let thermoLists: [ThermostatModel]
        let onReloadFeed: (Bool) -> Void
    }
    
    func map(state: TState, dispatch: @escaping DispatchFunction) -> Props {
        return Props(thermoLists: state.feeds, onReloadFeed: { reload in
            dispatch(TAction.Refresh(forceLoad: reload))
        })
    }
    
    func body(props: Props) -> some View {
        List {
            ForEach(props.thermoLists) { FeedRow(feed: $0) }
        }
        .onAppear {
            props.onReloadFeed(true)
        }
    }
    
}

extension ThermostatModel: Identifiable { }
